# LibreCube Board Template

The building block for all kinds of electrical systems for is the PC/104-alike
printed circuit board. This repository holds the electrical and mechanical
template of such board to use for your project.

![](docs/pcb_template_top.jpg)

## Documentation

Find the full board specification here:
https://librecube.gitlab.io/standards/board_specification/

## Contribute

- Issue Tracker: https://gitlab.com/librecube/support/templates/librecube-board/-/issues
- Source Code: https://gitlab.com/librecube/support/templates/librecube-board

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://gitlab.com/librecube/guidelines).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the CERN Open Hardware license. See the [LICENSE](./LICENSE.txt) file for details.
